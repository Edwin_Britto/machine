//
//  UsersResponse.swift
//  machine
//
//  Created by Macroid on 6/20/20.
//  Copyright © 2020 Macroid. All rights reserved.
//

import Foundation

struct UsersResponse:Codable {
    var result:[User]?
    
    enum CodingKeys: String, CodingKey {
        case result = "result"
    }
    
    init(from decoder:Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        result = try values.decodeIfPresent([User].self, forKey: .result)
    }
}


struct User:Codable {
    var id:Int?
    var first_name:String?
    var last_name:String?
    var birthday:String?
    var age:String?
    var email:String?
    var mobile_no:String?
    var address:String?
    var contact_person:String?
    var cp_mobile_no:String?
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case first_name = "firstname"
        case last_name = "lastname"
        case birthday = "birthday"
        case age = "age"
        case email = "email"
        case mobile_no = "mobileno"
        case address = "address"
        case contact_person = "contactperson"
        case cp_mobile_no = "cpmobileno"
    }
    
    init(from decoder:Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        first_name = try values.decodeIfPresent(String.self, forKey: .first_name)
        last_name = try values.decodeIfPresent(String.self, forKey: .last_name)
        birthday = try values.decodeIfPresent(String.self, forKey: .birthday)
        age = try values.decodeIfPresent(String.self, forKey: .age)
        email = try values.decodeIfPresent(String.self, forKey: .email)
        mobile_no = try values.decodeIfPresent(String.self, forKey: .mobile_no)
        address = try values.decodeIfPresent(String.self, forKey: .address)
        contact_person = try values.decodeIfPresent(String.self, forKey: .contact_person)
        cp_mobile_no = try values.decodeIfPresent(String.self, forKey: .cp_mobile_no)
    }
}
