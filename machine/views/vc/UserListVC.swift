//
//  ViewController.swift
//  machine
//
//  Created by Macroid on 6/20/20.
//  Copyright © 2020 Macroid. All rights reserved.
//

import UIKit
import CoreData

class UserListVC: UIViewController {

    @IBOutlet weak var user_table_view:UITableView!
    var managedObjectContext:NSManagedObjectContext?
    
    var users_list:[Users] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        
        let delegate = UIApplication.shared.delegate as? AppDelegate
        managedObjectContext = delegate?.managedObjectContext
        
        let nib = UINib(nibName: "UserListCell", bundle: nil)
        user_table_view.register(nib, forCellReuseIdentifier: "UserListCell")
        
        
        if let users_data = DBHelper().getExistingUsersInfo(objectContext: managedObjectContext), users_data.count > 0{
            users_list = users_data
            refreshTableView()
        }else{
            getAllUsers()
        }
        
        self.title = "Users List"
    }
    
    func refreshTableView(){
        user_table_view.delegate = self
        user_table_view.dataSource = self
        user_table_view.reloadData()
    }

}

extension UserListVC:UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "UserListCell") as! UserListCell
        let user = users_list[indexPath.row]
        cell.lbl_first_name.text = user.first_name
        cell.lbl_email.text = user.email
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return users_list.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 140
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let user = users_list[indexPath.row]
        showDetaiedVC(userInfo: user)
    }
    
    func showDetaiedVC(userInfo:Users){
        let vc = storyboard?.instantiateViewController(withIdentifier: "DetailedUserInfoVC") as! DetailedUserInfoVC
        vc.selected_user_info = userInfo
        navigationController?.pushViewController(vc, animated: true)
    }
}

//api call
extension UserListVC{
    //to get all users information
    func getAllUsers(){
        self.customActivityIndicatory(self.view, startAnimate: true)
        APIController.getAllUsersAPI(completion: { (request_status,users_info)in
            DispatchQueue.main.async {
                self.customActivityIndicatory(self.view, startAnimate: false)
            }
            if request_status{
                if let users_list = users_info, users_list.count > 0{
                    DispatchQueue.main.async {
                        self.users_list = DBHelper().setUserData(live_user_list: users_list,
                                                                 objectContext: self.managedObjectContext)
                        self.refreshTableView()
                    }
                }
            }else{
                print("something went wrong")
            }
        })
    }
}

