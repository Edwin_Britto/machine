//
//  DetailedUserInfoVC.swift
//  machine
//
//  Created by Macroid on 6/20/20.
//  Copyright © 2020 Macroid. All rights reserved.
//

import UIKit

class DetailedUserInfoVC: UIViewController {

    @IBOutlet weak var detailed_table_view:UITableView!
    
    var selected_user_info:Users?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let nib = UINib(nibName: "DetailedUserCell", bundle: nil)
        detailed_table_view.register(nib, forCellReuseIdentifier: "DetailedUserCell")
        
        self.title = selected_user_info?.first_name ?? ""
    }

}

extension DetailedUserInfoVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DetailedUserCell") as! DetailedUserCell
        cell.lbl_first_name.text = selected_user_info?.first_name ?? ""
        cell.lbl_last_name.text = selected_user_info?.last_name ?? ""
        cell.lbl_birth_day.text = selected_user_info?.birthday ?? ""
        cell.lbl_age.text = selected_user_info?.age ?? ""
        cell.lbl_email.text = selected_user_info?.email ?? ""
        cell.lbl_mobile.text = selected_user_info?.mobile_no ?? ""
        cell.lbl_contact_person.text = selected_user_info?.contact_person ?? ""
        cell.lbl_cp_mobile.text = selected_user_info?.cp_mobile_no ?? ""
        cell.lbl_address.text = selected_user_info?.address ?? ""
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 480
    }
}
