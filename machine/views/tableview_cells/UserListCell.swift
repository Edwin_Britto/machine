//
//  UserListCell.swift
//  machine
//
//  Created by Macroid on 6/20/20.
//  Copyright © 2020 Macroid. All rights reserved.
//

import UIKit

class UserListCell: UITableViewCell {

    @IBOutlet weak var lbl_first_name:UILabel!
    @IBOutlet weak var lbl_email:UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
