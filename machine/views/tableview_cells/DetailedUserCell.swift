//
//  DetailedUserCell.swift
//  machine
//
//  Created by Macroid on 6/20/20.
//  Copyright © 2020 Macroid. All rights reserved.
//

import UIKit

class DetailedUserCell: UITableViewCell {

    @IBOutlet weak var lbl_first_name:UILabel!
    @IBOutlet weak var lbl_last_name:UILabel!
    @IBOutlet weak var lbl_birth_day:UILabel!
    @IBOutlet weak var lbl_age:UILabel!
    @IBOutlet weak var lbl_email:UILabel!
    @IBOutlet weak var lbl_mobile:UILabel!
    @IBOutlet weak var lbl_address:UILabel!
    @IBOutlet weak var lbl_contact_person:UILabel!
    @IBOutlet weak var lbl_cp_mobile:UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
