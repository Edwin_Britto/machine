//
//  Users+CoreDataProperties.swift
//  
//
//  Created by Macroid on 6/20/20.
//
//

import Foundation
import CoreData


extension Users {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Users> {
        return NSFetchRequest<Users>(entityName: "Users")
    }

    @NSManaged public var age: String?
    @NSManaged public var birthday: String?
    @NSManaged public var id: Int16
    @NSManaged public var cp_mobile_no: String?
    @NSManaged public var email: String?
    @NSManaged public var first_name: String?
    @NSManaged public var last_name: String?
    @NSManaged public var contact_person: String?
    @NSManaged public var mobile_no: String?
    @NSManaged public var address: String?

}
