//
//  DBHelper.swift
//  machine
//
//  Created by Macroid on 6/20/20.
//  Copyright © 2020 Macroid. All rights reserved.
//

import Foundation
import CoreData

class DBHelper{
    
    func getExistingUsersInfo(objectContext:NSManagedObjectContext?)->[Users]?{
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Users")
            do{
                if let users = try objectContext?.fetch(fetchRequest) as? [Users]{
                    return users
                }else{
                    return nil
                }
            }catch{
                fatalError("fetch error")
            }
    }
    
    func setUserData(live_user_list:[User], objectContext:NSManagedObjectContext?)->[Users]{
        guard let entity = NSEntityDescription.entity(forEntityName: "Users", in: objectContext!) else{
            fatalError("entity error")
        }
        var users_list:[Users] = []
        for user in live_user_list{
         do{
            let local_user = Users(entity: entity, insertInto: objectContext!)
            local_user.id = Int16(user.id ?? 0)
            local_user.first_name = user.first_name
            local_user.last_name = user.last_name
            local_user.address = user.address
            local_user.age = user.age
            local_user.cp_mobile_no = user.cp_mobile_no
            local_user.email = user.email
            local_user.birthday = user.birthday
            local_user.contact_person = user.contact_person
            try objectContext?.save()
            users_list.append(local_user)
            }catch{
                fatalError("insert error")
            }
        }
        return users_list
    }
}
