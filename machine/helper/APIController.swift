//
//  APIController.swift
//  machine
//
//  Created by Macroid on 6/20/20.
//  Copyright © 2020 Macroid. All rights reserved.
//

import Foundation
import Alamofire

class APIController: NSObject{
    
    class func getAllUsersAPI(completion: @escaping(_ request_status:Bool, _ users_info:[User]?)->Void){
     
        Alamofire.request(BASE_URL + GET_ALL_USERS, method: .get).responseJSON{ response in
            switch response.result{
            case .failure:
                print("something error")
                completion(false,nil)
            case .success:
                print("success")
                do{
                    let users_response = try JSONDecoder().decode(UsersResponse.self, from: response.data!)
                    completion(true,users_response.result)
                }catch{
                    
                }
            }
        }
    }
}
