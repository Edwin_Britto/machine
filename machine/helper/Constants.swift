//
//  Constants.swift
//  machine
//
//  Created by Macroid on 6/20/20.
//  Copyright © 2020 Macroid. All rights reserved.
//

import Foundation

let BASE_URL = "https://us-central1-fwd-api.cloudfunctions.net/"

//get all users
let GET_ALL_USERS = "getUsersList"
